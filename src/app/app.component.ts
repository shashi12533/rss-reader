import { Component, OnInit } from '@angular/core';
import { FeedService } from './feed.service';
import { FeedEntry } from './model/feed-entry';

// Add the RxJS Observable operators we need in this app.
//import './rxjs-operators';
import 'rxjs/Rx';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private feedUrl: string = 'http://feeds.bbci.co.uk/news/rss.xml';
  feeds: Array<FeedEntry> = [];
  textValue = '';
  url:any;

  constructor (
    private feedService: FeedService
  ) {}

  ngOnInit() {
    this.refreshFeed();
  }

  refreshFeed() {
    this.feeds.length = 0;
    // Adds 1s of delay to provide user's feedback.
    this.feedService.getFeedContent(this.feedUrl).delay(1000)
        .subscribe(
            feed => this.feeds = feed.items,
            error => console.log(error));
  }
   refreshFeedler(url) {
    this.feeds.length = 0;
    // alert(url);
    // Adds 1s of delay to provide user's feedback.
    this.feedService.getFeedContent(url).delay(1000)
        .subscribe(
            feed => this.feeds = feed.items,
            error => console.log(error));
  }

}
